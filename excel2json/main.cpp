//
//  main.m
//  excel2json
//
//  Created by Jason on 16-3-10.
//  Copyright (c) 2016年 Jason. All rights reserved.
//

#include "Xls2Json.h"

int main(int argc, const char * argv[]) {
    Xls2Json* loader = new Xls2Json();
    loader->load_from_cfg();
    delete loader;
    getchar();
    return 0;
}
