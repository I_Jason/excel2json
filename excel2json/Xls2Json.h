// 王智泉

#include <vector>
#include <map>
#include <string>
#include "jsoncpp/include/json/json.h"

typedef std::vector<std::string> FiledNames;

namespace YExcel
{
    class BasicExcelWorksheet;
}

class Xls2Json
{
public:
    Xls2Json(void);
    virtual ~Xls2Json(void);
    
    // 从配置文件加载
    void load_from_cfg();
    
private:
    
    void to_json(const char* xlsFile);
    
    void to_xls(const char* jsonFile);
    
    void parser_sheet(YExcel::BasicExcelWorksheet* sheet);
    
    // 获得指定表的字段名列表
    FiledNames get_fileds(YExcel::BasicExcelWorksheet* sheet, int skipLine = 0);
    
    int read_datas(YExcel::BasicExcelWorksheet* sheet, const FiledNames& fileds, Json::Value* data);
    
private:
    
    std::map<size_t, bool> m_json_tags;
    std::string m_to_dir;
    size_t      m_skip;     // 除字段名外，跳过多少行，跳过的行可以用作对字段的说明
    std::size_t m_max_rows;
    std::size_t m_max_cols;
};